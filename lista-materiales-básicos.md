# Introducción
En este documento encontrarás un listado de materiales básicos para ayudar a los participantes a crear sus robots.

# Listado de materiales
* [ ] Bridas
* [ ] Cinta americana 
* [ ] ¿Grapadoras?
* [ ] Pistolas de cola caliente y barras de cola caliente (a más mejor)
* [ ] Restos de cartón (idealmente en formato caja y en formato plancha)
* [ ] Restos de juguetes electrónicos (coches a control, helicopteros... cualquier cosa que gire es bienvenida)
* [ ] Tijeras de las que tienen punta plana (para que puedan cortar también los niños sin problemas)
* [ ] Pequeños motores (ideales motores DC 5v con reductora)
* [ ] Cables
* [ ] Portapilas (de 4 pilas a dos cables) Alternativamente, pilas de petaca de 4,5 o powerbanks o otras cosas para dar corriente a los motores.
* [ ] Rotuladores de colores para decorar los robots
* [ ] Chorradas varias para decorar los robots (ej, bolas de navidad, cintas hawaianas, etc.)
* [ ] Pelotas de ping pong
* [ ] Post-its para organizar equipos y votaciones varias
* [ ] Cinta conductora para hacerse botones
* [ ] Pinzas de la ropa

# Colabora
¿Se nos ha olvidado algo? Aporta tus ideas al listao de materiales básicos para celebrar una Hebocon legendaria.